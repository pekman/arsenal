#!/bin/sh

# Descrição		-> xbps-notify.sh - Verifica os repsoitórios e exibe uma notificação com possíveis atualizações
# Link				-> https://codeberg.org/pekman/arsenal/src/branch/main/sistema/xbps/xbps-notify.sh
# Autor			-> pekman
# Site				-> https://codeberg.org/pekman
# Git 				-> https://codeberg.org/pekman
# Mastodon		-> https://mas.to/@pekman
# Licença			-> GPL3
# Criado			-> 30.03.2021 19:43:26
# Atualizado		-> 01.04.2021 21:16:09
# IRC 				-> Rede: Freenode || Canal: #gnl

# Dependências: libnotify, daemon de notificação (dunst, statnot, xfce4-notifyd, lxqt-notificationd, mako, mate-notification-daemon, notify-osd ...)

XBPS_NOTIFY() {
	updates=$(xbps-install -Mun 2> /dev/null | wc -l)

	if [ -n "$updates" ] && [ "$updates" -gt 0 ]; then
		notify-send -u critical "📢 Há [$updates] pacote(s) para atualizar 🔄"
	else
		notify-send "👍 Seu sistema está atualizado 🙏"
	fi
}
while :; do
  XBPS_NOTIFY
  sleep 1h
  XBPS_NOTIFY
done
